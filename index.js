var jwt = require('jsonwebtoken');
var httperror = require('http-errors');

const token_verification = async function (token, tenantList = [], key) {
  try {
    const date = new Date();
    const currentTime = parseInt(date.getTime()/1000);
    const secretKey = key || process.env.AUTHSECRET;
    if (!key) throw new httperror(400, 'Empty JWT Authentication key!');
    const tokenPayload = jwt.verify(token, secretKey);
    if (tokenPayload.expires < currentTime) throw httperror(401, 'Token expired!');
    if (!tenantList.indexOf(tokenPayload.tenant) > -1) throw (401, 'Unauthorized access!');
    return {
      userId: tokenPayload.id || null,
      email: tokenPayload.email || null,
      userName: tokenPayload.username || null,
      expires: tokenPayload.expires || null,
      tenant: tokenPayload.tenant || null
    }
  } catch (err) {
    throw new httperror(401, err.message);
  }
}

exports.verify = token_verification;